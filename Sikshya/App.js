import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import home from './container/home.js';
import calendar from './container/calendar.js';

const AppNavigator = StackNavigator({
  SettingScreen: { screen: home },
  HomeScreen: { screen: calendar }
});

export default class App extends Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}